import typer

from wuggy.plugins import pm, enabled_plugins_info


def main(verbose: bool = typer.Option(False, "--verbose", "-v")):
    if verbose:
        typer.echo(enabled_plugins_info())

    load_results = pm.hook.load_data(source="foobar")
    typer.echo(load_results)

    display_results = pm.hook.display_data(data="wangbang")
    typer.echo(display_results)


def cli():
    typer.run(main)


if __name__ == "__main__":
    typer.run(main)
