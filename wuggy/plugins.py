import importlib
import pluggy
import sys
from . import hookspecs
from pathlib import Path
import os

from .settings import PLUGINS_ENABLED_DIR, APP_NAME


def stem(fn: str) -> str:
    """Return the filename without the extension"""
    return Path(fn).stem


def plugins_enabled() -> list[str]:
    """Return a list of plugin names from PLUGINS_ENABLED_DIR"""
    return [
        f"{PLUGINS_ENABLED_DIR}.{stem(filename)}"
        for filename in os.listdir(PLUGINS_ENABLED_DIR)
        if (filename.endswith(".py") and not filename.startswith("__"))
    ]


def enabled_plugins():
    """Return a sprted list of enabled plugins sorted by filename"""
    return sorted(pm.get_plugins(), key=lambda p: p.__name__)


def enabled_plugins_info() -> list[dict]:
    plugins = []
    plugin_to_distinfo = dict(pm.list_plugin_distinfo())
    for plugin in pm.get_plugins():
        plugin_info = {
            "name": plugin.__name__,
            "hooks": [h.name for h in pm.get_hookcallers(plugin)],
        }
        if distinfo := plugin_to_distinfo.get(plugin):
            plugin_info["version"] = distinfo.version
            plugin_info["name"] = distinfo.project_name
        plugins.append(plugin_info)
    return plugins


pm = pluggy.PluginManager(APP_NAME)
pm.add_hookspecs(hookspecs)

if not hasattr(sys, "_called_from_test"):
    # Only load plugins if not running tests
    pm.load_setuptools_entrypoints(APP_NAME)


for plugin in plugins_enabled():
    mod = importlib.import_module(plugin)
    pm.register(mod, plugin)
