import asyncio


def example():
    return "example text"


async def await_me_maybe(value):
    """If value is callable, call it.
    If awaitable, await it. Otherwise return it.
    # https://simonwillison.net/2020/Sep/2/await-me-maybe/
    """
    if callable(value):
        value = value()
    if asyncio.iscoroutine(value):
        value = await value
    return value
