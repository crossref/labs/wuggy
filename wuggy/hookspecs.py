from pluggy import HookimplMarker
from pluggy import HookspecMarker

hookspec = HookspecMarker("wuggy")
hookimpl = HookimplMarker("wuggy")


@hookspec
def title():
    """Return the title of the plugin"""


@hookspec
def description():
    """Return the description of the plugin"""


@hookspec
def version():
    """Return the version of the plugin"""


@hookspec
def load_data(source: str):
    """Load the data

    :param source: where to get the data
    :return: the data
    """


@hookspec
def display_data(data: dict):
    """Display the data"""
