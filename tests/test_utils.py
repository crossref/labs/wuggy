import pytest


async def async_thingy():
    return "async thingy"


def thingy():
    return "sync thingy"


def test_example():
    from wuggy.utils import example

    assert example() == "example text"


@pytest.mark.asyncio
async def test_await_me_maybe():
    from wuggy.utils import await_me_maybe

    assert await await_me_maybe(async_thingy) == "async thingy"
    assert await await_me_maybe(thingy) == "sync thingy"
