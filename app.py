import logging
import streamlit as st
from wuggy.plugins import enabled_plugins


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def show_splash():
    st.header("Splash Screen")


def show_results():
    st.header("Results Screen")
    plugins = enabled_plugins()
    tab_names = [plugin.title() for plugin in plugins]

    containers = st.tabs(tab_names)
    for index, container in enumerate(containers):
        container.write(f"Title: {plugins[index].title()}")
        container.write(f"Description: {plugins[index].description()}")
        container.write(f"Version: {plugins[index].version()}")
        container.write(f"Display: {plugins[index].display_data({'test': 'data'})}")


def update_sidebar():
    st.sidebar.header("Sidebar")
    st.sidebar.text("This is a sidebar")
    st.sidebar.button("Click me", key="clickme")


logger.info("Loading streamlit")

st.set_page_config(page_title="Wuggy App", page_icon=None, layout="wide")

if "exisiting_session" in st.session_state:
    logger.info("Continuing session")
    show_results()
else:
    logger.info("New session")
    st.session_state.exisiting_session = True
    show_splash()

update_sidebar()
logger.info("Done")
