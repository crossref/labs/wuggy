# Wuggy

Wuggy is an example application to show how to use dynamcally loaded plugins in a Python project.

## Installation

[assumes for development]

Get the code, create and activate a virtual evironment.

- `git clone git@gitlab.com:crossref/labs/wuggy.git`
- `cd wuggy`
- `python -m venv venv && . ./venv/bin/activate`

Install the app and its development dependencies in editable mode

- `pip install -e .`
- `pip install -e '.[dev]'`

The repo includes a `Justfile` for managing development tasks with [Just](https://github.com/casey/just).

To install just on MacOS:

- `brew install just`

To install just on Linux:

- `sudo apt install just`


Once `just`` is installed, you can run things like:

- `just test` to run tests
- `just lint` to check code style and formatting
- `just fix` to auto-fix linting issues

## Running

There are two examples of using the framework and plugins

- wug: a CLI application that loads plugins
- app.py: and example streamlit applicatiion that loads plugins and chnages its display based on plugins

To run the CLI app:

- `wug`

To run the streamlit app:

- `streamlit run app.py`

## Understanding what is going on

Plugins are just Python modules that work with [pluggy](https://github.com/pytest-dev/pluggy). The main point of this example is to who how you can dynamically load and invoke plugins from a directory.

The interface for a wuggy plugin is defined in `wuggy/hookspecs.py.`

Both the `wug` CLI and `app.py` examples load plugins from the `plugins_enabled` directory.

The plugins in the `plugins_enabled` directory are simply softlinks to real Python modules in either `plugins_available/crossref/` or `plugins_available/community/`.

If you want plugins to load in a particular order- prefix them with the order number you want them loaded in when you create the links.

If you are familiar with how Apache2 is setup in Ubuntu-based distros, this convention for managing plugins may be familiar.

Look at `plugins_available/community/piglatin.py` for an example plugin implementation. Note that it also includes tests.

To install a new plugin:

- copy the plugin to either `plugins_available/crossref` or `plugins_available/community` as appropriate.
- soft link the plugin that you want to enable into `plugins_enabled`. For example: `ln -s plugins_avaliable/community/piglatin.py plugins_enabled/04_piglatin.py`
