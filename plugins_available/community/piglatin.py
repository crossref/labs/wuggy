from wuggy.hookspecs import hookimpl


def piglatin(sentence: str) -> str:
    """Return the piglatin version of a sentence"""
    words = sentence.split()
    new_words = []
    for word in words:
        if word[0] in "aeiou":
            new_words.append(word + "way")
        else:
            new_words.append(word[1:] + word[0] + "ay")
    return " ".join(new_words)


@hookimpl
def title():
    return "Pig Latin"


@hookimpl
def description():
    return "Translate member name to Pig Latin"


@hookimpl
def version():
    return "1.0.0"


@hookimpl
def display_data(data: dict):
    return piglatin(description())


def test_piglatin():
    test_sentence = "hello world"
    expected = "ellohay orldway"
    result = piglatin(test_sentence)
    assert result == expected
