from wuggy.hookspecs import hookimpl


@hookimpl
def load_data(source: str):
    return f"Loading Sugar for {source}"


@hookimpl
def cdisplay_data(data: dict):
    return f"Displaying Sugar data: {data}"
