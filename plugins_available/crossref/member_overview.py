from wuggy.hookspecs import hookimpl


@hookimpl
def title():
    return "Overview"


@hookimpl
def description():
    return "Overview of member data"


@hookimpl
def version():
    return "1.0.0"


@hookimpl
def load_data(source: str):
    return f"Loading overview for {source}"


@hookimpl
def display_data(data: dict):
    return f"Displaying overview data: {data}"
