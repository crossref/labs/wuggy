from wuggy.hookspecs import hookimpl


@hookimpl
def title():
    return "TLDs"


@hookimpl
def description():
    return "Top Level Domains (TLDs) used by the member"


@hookimpl
def version():
    return "1.0.0"


@hookimpl
def load_data(source: str):
    return f"Loading TLDs for {source}"


@hookimpl
def display_data(data: dict):
    return f"Displaying TLD data: {data}"
