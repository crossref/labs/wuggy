from wuggy.hookspecs import hookimpl


@hookimpl
def load_data(source: str):
    return f"Loading Resolutions for {source}"


@hookimpl
def display_data(data: dict):
    return f"Displaying Reolutions data: {data}"
